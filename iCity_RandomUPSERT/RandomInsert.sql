CREATE PROCEDURE [dbo].[iCity_insert_productTbl]
AS

DECLARE @Randomprice INT;
DECLARE @Upperpr INT;
DECLARE @Lowerpr INT;

DECLARE @Randomname INT;
DECLARE @Uppername INT;
DECLARE @Lowername INT;

DECLARE @Randomtype INT;
DECLARE @Uppertype INT;
DECLARE @Lowertype INT;

DECLARE @Randomav INT;
DECLARE @Upperav INT;
DECLARE @Lowerav INT;

Declare @Ppname VARCHAR(100);
DECLARE @Pptype VARCHAR(100);

 
---- This will create a random number between 1 and 999

declare @pnamer int
declare @ptype int
declare @counter int
declare @av VARCHAR(1)

declare @i int 
DECLARE @hp int
DECLARE @lp int
SELECT @hp=MAX(product_cost) from product
SELECT @lp=MIN(product_cost) from product

select @i = MAX(id) from product
SELECT @counter = 1
while @counter >=1 and @counter <= 1000
begin
	SET @Lowerpr = @lp+2000 ---- The lowest random number
	SET @Upperpr = @hp+2000 ---- The highest random number
	SELECT @Randomprice = ROUND(((@Upperpr - @Lowerpr -1) * RAND() + @Lowerpr), 0)
	SELECT @Lowerpr = @Lowerpr + 500
	SELECT @Upperpr = @Upperpr + 500
--SELECT @Randomprice

	SET @Lowername = 0 ---- The lowest random number
	SET @Uppername = 6 ---- The highest random number
	SELECT @Randomname = ROUND(((@Uppername - @Lowername -1) * RAND() + @Lowername), 0)

	SET @Lowertype = 0 ---- The lowest random number
	SET @Uppertype = 5 ---- The highest random number
	SELECT @Randomtype = ROUND(((@Uppertype - @Lowertype -1) * RAND() + @Lowertype), 0)

	SET @Lowerav = 0 ---- The lowest random number
	SET @Upperav = 9 ---- The highest random number
	SELECT @Randomav = ROUND(((@Upperav - @Lowerav -1) * RAND() + @Lowerav), 0)

	IF (@Randomname=1)
	SELECT @Ppname='TV'
	IF (@Randomname=2)
	SELECT @Ppname='Fridge'
	IF (@Randomname=3)
	SELECT @Ppname='AC'
	IF (@Randomname=4)
	SELECT @Ppname='Mobile'
	IF (@Randomname=5)
	SELECT @Ppname='Laptop' ;

	IF (@Randomtype=1)
	SELECT @Pptype='electronics';
	IF (@Randomtype=2)
	SELECT @Pptype='Accessories';
	IF (@Randomtype=3)
	SELECT @Pptype='Gadgets';
	IF (@Randomtype=4)
	SELECT @Pptype='Kitchen';

	IF (@Randomav=1)
	SELECT @av='n'
	IF (@Randomav=2)
	SELECT @av='y'
	IF (@Randomav=3)
	SELECT @av='y'
	IF (@Randomav=4)
	SELECT @av='y'
	IF (@Randomav=5)
	SELECT @av='y'
	IF (@Randomav=6)
	SELECT @av='y'
	IF (@Randomav=7)
	SELECT @av='y'
	IF (@Randomav=8)
	SELECT @av='y'

	DECLARE @today datetime
	SELECT @today = getdate()
    insert into Production.dbo.product values (@i +1, @Ppname, @Pptype ,@Randomprice,@av,@today,@today)

	SELECT @counter = @counter + 1
	select @i = @i + 1
end


GO

