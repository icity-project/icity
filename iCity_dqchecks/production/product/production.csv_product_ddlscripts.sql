
--DDL FOR production.csv_product

--Staging Table
CREATE TABLE IF NOT EXISTS production.csv_product_staging(
		mod_val VARCHAR (256),
		id bigint, 
		product_name varchar(256), 
		product_type varchar(256), 
		product_cost bigint, 
		product_availability varchar(256), 
		create_date varchar(256), 
		updated_date varchar(256));
 
--Target Table
CREATE TABLE IF NOT EXISTS production.csv_product(
		id BIGINT,
		product_name varchar(256),
		product_type varchar(256),
		product_cost BIGINT,
		product_availability varchar(256),
		create_date TIMESTAMP, 
		updated_date TIMESTAMP);


-- Script to insert the data into the Final target table in Postgress
INSERT INTO production.csv_product(
		id, 
		product_name, 
		product_type, 
		product_cost, 
		product_availability, 
		create_date, 
		updated_date)
		(SELECT 
		id::BIGINT, 
		product_name, 
		product_type, 
		product_cost::BIGINT,
		product_availability, 
		create_date::timestamptz, 
		updated_date::timestamptz
		FROM production.csv_product_staging)

--DQ Check DDL

--DDL for checking Count 
SELECT COUNT(*) FROM production.csv_product_staging;

--DDL for Checking Duplicates
SELECT id, product_name, product_type, product_cost, product_availability, create_date, updated_date, COUNT(*) AS COUNT 
FROM production.csv_product_staging GROUP BY id, product_name, product_type, product_cost, 
product_availability, create_date, updated_date HAVING COUNT(*) > 1;

--DDL for Top Fifty and Bottom Fifty
SELECT *,ROW_NUMBER() OVER (ORDER BY ID)AS Topfifty 
FROM production.csv_product_staging LIMIT 50;

SELECT *,ROW_NUMBER() OVER (ORDER BY ID DESC)AS Bottomfifty 
FROM production.csv_product_staging LIMIT 50;



--Deduplication
CREATE TABLE production.csv_product_staging_deduplicated AS
SELECT
id,
product_name,
product_type,
product_cost,
product_availability,
create_date, 
updated_date
FROM production.csv_product_staging
GROUP BY
id,
product_name,
product_type,
product_cost,
product_availability,
create_date, 
updated_date;



