import sys
import logging
from datetime import datetime 
import icity_aws_utils as utils

BASE_DQ_DIRECTORY = 's3://icity-dqcheck/'
COLLECTION_DQ_DIRECTORY = 'production/csv_product'
SNS_SUBJECT = 'DQ Check Status for production-csv_product'

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def deduplication():
    '''query to remove duplicates 
    from the stagetable using CTAS '''
    
    deduplication_query = """CREATE TABLE production.csv_product_staging_deduplicated AS
        SELECT
        id,
        product_name,
        product_type,
        product_cost,
        product_availability,
        create_date, 
        updated_date
        FROM production.csv_product_staging
        GROUP BY
        id,
        product_name,
        product_type,
        product_cost,
        product_availability,
        create_date, 
        updated_date;"""
        
    insert_query = """INSERT INTO production.csv_product(
		id, 
		product_name, 
		product_type, 
		product_cost, 
		product_availability, 
		create_date, 
		updated_date)
		(SELECT 
		id::BIGINT, 
		product_name, 
		product_type, 
		product_cost::BIGINT,
		product_availability, 
		create_date::timestamptz, 
		updated_date::timestamptz
		FROM production.csv_product_staging_deduplicated)"""
        
    query_drop_table = """drop table production.csv_product_staging_deduplicated"""
    query_truncate_table = """TRUNCATE production.csv_product_staging"""
    
    utils.execute_query(cursor, deduplication_query, False)
    utils.execute_query(cursor, insert_query, False)
    utils.execute_query(cursor, query_truncate_table, False)
    utils.execute_query(cursor, query_drop_table, False)
    
    return
    
def get_staging_to_prod_query():
    '''Query to insert data from staging table to target
     table as a final step
    '''
    insert_query = """INSERT INTO production.csv_product(
        id, 
        product_name, 
        product_type, 
        product_cost, 
        product_availability, 
        create_date, 
        updated_date)
        (SELECT 
        id::BIGINT, 
        product_name, 
        product_type, 
        product_cost::BIGINT,
        product_availability, 
        create_date::timestamptz, 
        updated_date::timestamptz
        FROM production.csv_product_staging)"""

    return insert_query
    
def lambda_handler(event, context):
    '''
    Main function that performs the DQ checks
    :param event:
        Use to pass parameters to lambda function e.g. process_date
    :param context:
        Internal to AWS lamda
    '''    

    try:
        #Set the subject for appropiate SNS notification
        utils.set_sns_subject(SNS_SUBJECT)

        #Fetch postgress credentials using secretsmanager
        postgres_credentials = utils.get_postgres_credentials()
        #Connect to postgress using psycopg2
        conn, cursor = utils.get_postgres_connection(postgres_credentials)
        
        process_date = datetime.now().strftime("%Y-%m-%d")
        
        s3_results_path = COLLECTION_DQ_DIRECTORY + "/" + process_date
        
        ##Start of DQ check & merge process

        #1. Query to check count of records
        query_record_count = "SELECT COUNT(*) FROM production.csv_product_staging"

        #2. Query to check for duplicate records
        query_dup_check = """SELECT id, product_name, product_type, product_cost, 
            product_availability, create_date, updated_date, COUNT(*) AS COUNT 
            FROM production.csv_product_staging GROUP BY id, product_name, product_type, product_cost, 
            product_availability, create_date, updated_date HAVING COUNT(*) > 1;"""

        #3. Query to test loading TOP 50 recrods
        query_top_fifty_records = """SELECT *,ROW_NUMBER() OVER (ORDER BY ID)AS Topfifty 
            FROM production.csv_product_staging LIMIT 50;"""

        #4. Query to test loading BOTTOM 50 records
        query_bottom_fifty_records = """SELECT *,ROW_NUMBER() OVER (ORDER BY ID DESC)AS Bottomfifty 
            FROM production.csv_product_staging LIMIT 50;"""

        #5. Query to move data from staging to production
        query_insert = get_staging_to_prod_query()
        
        #6. Query to drop Staging table
        query_truncate_table = """TRUNCATE production.csv_product_staging"""
        
        dupes = utils.do_duplicate_check(cursor, query_dup_check, s3_results_path + "/Duplicates")
        if not dupes:
            deduplication()
        utils.do_record_count_check(cursor, query_record_count, s3_results_path + "/Count")
        utils.do_data_load_check(cursor, query_top_fifty_records, s3_results_path + "/Top50")
        utils.do_data_load_check(cursor, query_bottom_fifty_records, s3_results_path + "/Bottom50")

        logger.info("DQ Check has passed, moving data from staging to prod area")
        
        utils.execute_query(cursor, query_insert, False)

        #Query to truncate the stage table
        #utils.execute_query(cursor, query_truncate_table, False)
        conn.commit()

        #Closing the connection
        cursor.close()
        conn.close()

        message='DQ Check Passed, DQ check results stored into ' +\
            BASE_DQ_DIRECTORY + COLLECTION_DQ_DIRECTORY +\
            'and data is loaded from stage table to target table'

        logger.info(message)
        utils.publish_to_sns(message, "PASSED")
        return
    except:
        failure_message = 'Exception encountered in DQ check [%s]' % sys.exc_info()[1]
        logger.error(failure_message)
        utils.publish_to_sns(failure_message, "FAILED")
        return

